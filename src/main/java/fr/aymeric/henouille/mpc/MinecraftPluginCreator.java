package fr.aymeric.henouille.mpc;

import fr.aymeric.henouille.mpc.stage.StageLoader;
import fr.aymeric.henouille.mpc.utils.VersionManager;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * MinecraftPluginCreator
 * this is the main class.
 */
public class MinecraftPluginCreator extends Application {

    /**
     * The name of the application.
     */
    public static final String APP_NAME = "Minecraft Plugin Creator";
    public static VersionManager versionManager;

    /**
     * Main function for javaFX application.
     * @param primaryStage The first created stage.
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        StageLoader main = new StageLoader("pages/main-page.fxml");
        main.open();
    }

    /**
     * Run the application.
     * @param args Arguments from the main function
     */
    public static void run(String[] args) {
        versionManager = new VersionManager();
        launch(args);
    }

    public static VersionManager getVersionManager() {
        return versionManager;
    }
}