package fr.aymeric.henouille.mpc.utils;

public class StringNormaliser {

    public static final String[] prohibited = {
            "<",
            "(",
            "[",
            "{",
            "\\",
            "^",
            "=",
            "$",
            "!",
            "|",
            "]",
            "}",
            ")",
            "?",
            "*",
            "+",
            ">",
            " "
    };

    public static String normalise(String toNormalise) {
        for (String str : prohibited) {
            toNormalise = toNormalise.replace(str, "");
        }
        return toNormalise;
    }

    public static String normaliseName(String toNormalise) {
        toNormalise = toNormalise.replace(" ", "-");
        return normalise(toNormalise);
    }

    public static String normaliseClass(String toNormalise) {
        String[] normalise = toNormalise.split("-");
        toNormalise = "";
        for (String arg : normalise) {
            toNormalise += (arg.charAt(0) + "").toUpperCase() + arg.substring(1);
        }
        return toNormalise;
    }

}
