package fr.aymeric.henouille.mpc.utils;

import java.io.*;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class FileInterpreter {

    public static final String DEFAULT_VARIABLE_DELIMITER = "%";

    protected InputStream fileInputStream;

    protected List<String> content;
    protected HashMap<String, String> variables;

    protected String variableDelimiter;


    public FileInterpreter(InputStream inputStream) {
        this.fileInputStream = inputStream;
        variableDelimiter = DEFAULT_VARIABLE_DELIMITER;
        variables = new HashMap<String, String>();
        read();
    }

    public void read() {
        content = new ArrayList<String>();

        try (
                BufferedInputStream bos = new BufferedInputStream(this.fileInputStream);
                Scanner scanner = new Scanner(bos);
        ) {

            while(scanner.hasNext()) {
                content.add(scanner.nextLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String generate() {
        String index = "";
        for (String line : content) {
            for (String key : variables.keySet()) {
                line = line.replace(variableDelimiter + key + variableDelimiter, variables.get(key));
            }
            index+=line+"\n";
        }
        return index;
    }

    public void addVariable(String name, String value) {
        variables.putIfAbsent(name, value);
    }

    public void setVariableDelimiter(String variableDelimiter) {
        this.variableDelimiter = variableDelimiter;
    }

    public String getVariableDelimiter() {
        return variableDelimiter;
    }

    public List<String> getContent() {
        return content;
    }
}