package fr.aymeric.henouille.mpc.utils;

import fr.aymeric.henouille.mpc.Main;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;

public class AssetManager {


    public InputStream getFileStream(String name) {
        return getAssetFile().getResourceAsStream("assets/" + name);
    }

    public ClassLoader getAssetFile() {
        return Main.class.getClassLoader();
    }

}
