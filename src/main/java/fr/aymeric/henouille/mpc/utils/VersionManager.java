package fr.aymeric.henouille.mpc.utils;

import java.io.FileInputStream;

public class VersionManager {

    private String appVersion;
    private String[] spigotVersion;

    public VersionManager() {
        AssetManager assetManager = new AssetManager();
        this.appVersion = new FileInterpreter(assetManager.getFileStream("version/app.version")).generate();
        this.spigotVersion = new FileInterpreter(assetManager.getFileStream("version/spigot.version")).generate()
                .split("\n");
    }

    public String getAppVersion() {
        return appVersion;
    }

    public String[] getSpigotVersion() {
        return spigotVersion;
    }

}
