package fr.aymeric.henouille.mpc.stage;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public class StageLoader {

    private Parent root;
    private Stage stage;

    public StageLoader(String xmlPath) {
        stage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(this.getClass().getClassLoader().getResource(xmlPath));
            root = loader.load();

            AtomicReference<Double> xOffset = new AtomicReference<>((double) 0);
            AtomicReference<Double> yOffset = new AtomicReference<>((double) 0);

            root.setOnMouseMoved(event -> {
                 xOffset.set(event.getSceneX());
                 yOffset.set(event.getSceneY());
            });

            root.setOnMouseDragged(event -> {
                stage.setX(event.getScreenX() - xOffset.get());
                stage.setY(event.getScreenY() - yOffset.get());
            });

            ((StageInjector) loader.getController()).setStage(stage);
            Scene scene = new Scene(root);
            scene.setFill(Color.TRANSPARENT);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void open() {
        stage.show();
    }

    public void close() {
        stage.close();
    }

    public Stage getStage() {
        return stage;
    }
}
