package fr.aymeric.henouille.mpc.project;

import fr.aymeric.henouille.mpc.utils.StringNormaliser;

public class Project {

    private String mainClassName;
    private String name;
    private String domain;
    private String packageProject;
    private String path;
    private String minecraftVersion;
    private String author;

    public Project(String name, String domain, String path, String minecraftVesion, String author) {
        this.name = StringNormaliser.normaliseName(name);
        this.mainClassName = StringNormaliser.normaliseClass(this.name);
        this.domain = StringNormaliser.normalise(domain.toLowerCase());
        this.path = path + "/" + mainClassName;
        this.minecraftVersion = minecraftVesion;
        this.packageProject = StringNormaliser.normalise(domain.toLowerCase() + "." + name.toLowerCase());
        this.author = author;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPackage(String packageProject) {
        this.packageProject = packageProject;
    }

    public void setMinecraftVersion(String minecraftVersion) {
        this.minecraftVersion = minecraftVersion;
    }

    public void setMainClassName(String mainClassName) {
        this.mainClassName = mainClassName;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMainClassName() {
        return mainClassName;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getDomain() {
        return domain;
    }

    public String getPackage() {
        return packageProject;
    }

    public String getMinecraftVersion() {
        return minecraftVersion;
    }

    public String getTestPackage() {
        return StringNormaliser.normalise(domain.toLowerCase() + ".test" + name.toLowerCase());
    }

    public String getAuthor() {
        return author;
    }
}
