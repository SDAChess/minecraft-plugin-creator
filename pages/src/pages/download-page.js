import { archives } from '../utils/download';
import $ from 'jquery';

$(() => {

    archives.forEach(elem => {
        ['zip', 'tar'].forEach(ext => {
            $('#download-table').append(`
                <tr>
                    <td>
                        ${elem.os}
                    </td>
                    <td>
                        ${elem.version}
                    </td>
                    <td>
                        <a href="res/${elem.os}/mpc-${elem.os}-${elem.version}.${ext}">
                            mpc-${elem.os}-${elem.version}.${ext}
                        </a>
                    </td>
                </tr>
            `);
        });
    })

});