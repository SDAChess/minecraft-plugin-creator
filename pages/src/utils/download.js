const version = require('../version.json').version;

export const archives = [
    { os: 'win', version },
    { os: 'linux', version },
    { os: 'mac', version }
];
